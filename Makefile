.POSIX:

PREFIX=/usr/local
MANPREFIX=$(PREFIX)/share/man

all:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f volsv $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/volsv
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp -f volsv.1 $(DESTDIR)$(MANPREFIX)/man1/volsv.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/volsv.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/volsv
	rm -f $(DESTDIR)$(MANPREFIX)/man1/volsv.1

.PHONY: all options clean dist install uninstall
