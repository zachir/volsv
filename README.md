# VolSV

Volume Setter V

#### What is it?

Volsv is a POSIX-compliant shell script I wrote in 2020, and have been modifying since. Presently, it supports:

- Increasing volume
- Decreasing volume
- Toggling the output mute
- Toggling the microphone mute
- Getting the volume
- Getting the mute state
- Getting both volume and mute state

in pipewire, pulseaudio, sndio, and ALSA. Once I get FreeBSD up and running, I will add those sound options too.

It depends on:

- pamixer (pulseaudio/pipewire)
- amixer (alsa/pipewire mic mute)
- mixerd (sndio)
- pgrep (with the -x command, standard in BSD and \*/Linux systems)

#### But *why* though?

I made volsv so that I could use it with keybindings to control the volume,
regardless of whether or not I was running pulseaudio, since I dislike
pulseaudio and tend to avoid it when possible. This means I can have a
consistent keybinding, whether or not I'm using it. Additionally, it has
provided a fun learning experience as
I learn to write scripts.

#### Flags/Commands

- increase volume: `-i`
- decrease volume: `-d`
- toggle system mute: `-t`
- toggle microphone mute: `-m`
- get volume level: `-v`
- get mute state: `-g`
- get volume and mute: `-a`
- help message: `-h`

